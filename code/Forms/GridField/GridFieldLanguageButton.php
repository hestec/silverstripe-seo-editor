<?php

class GridFieldLanguageButton implements GridField_HTMLProvider {
    protected $targetFragment;

    public function __construct($targetFragment = "before") {
        $this->targetFragment = $targetFragment;
    }

    public function getHTMLFragments($gridField) {

        if (class_exists('Translatable')) {

            $current_lang = i18n::get_locale();

            $langs = Translatable::get_allowed_locales();

            $dropdown = '<select class="" onChange="window.location.href=this.value">';
            foreach ($langs as $option) {

                if ($current_lang == $option) {
                    $selected = " selected";
                } else {
                    $selected = '';
                }
                $dropdown .= '<option value="/admin/seo-editor/?locale=' . $option . '"' . $selected . '>' . i18n::get_language_name(i18n::get_lang_from_locale($option)) . '</option>';

            }
            $dropdown .= '</select>';

            return array(
                $this->targetFragment => $dropdown,
            );

        }

    }

}